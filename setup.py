from setuptools import setup, find_packages

setup(
	name = "django-blog",
	version = "0.01",
	url = 'http://localhost',
	license = 'commerial',
	description = "A skeleton to use django",
	author = "Tomasz Cichecki",
	packages = find_packages('src'),
	package_dir = {"": 'src'},
	install_requires = ['setuptools', "django"]
)
